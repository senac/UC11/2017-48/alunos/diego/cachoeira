package br.com.senac.cachoeiras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnriodomeio;
    private Button btnmatilde;
    private Button btnveudenoiva;
    private Button btnpalito;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    btnriodomeio = findViewById(R.id.btnriodomeio);
    btnmatilde = findViewById(R.id.btnmatilde);
    btnveudenoiva = findViewById(R.id.btnveudenoiva);
    btnpalito = findViewById(R.id.btnpalito);


    btnriodomeio.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,RiodoMeioActivity.class);
            startActivity(intent);
        }
    });

    btnmatilde.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,MatildeActivity.class);
            startActivity(intent);
        }
    });
    btnveudenoiva.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,VeudenoivaActivity.class);
            startActivity(intent);
        }
    });
    btnpalito.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,PalitoActivity.class);
            startActivity(intent);
        }
    });





    }
}
